'''
This script is the one without using any Docker mechanism. Hence the name. Should be run individually without bothering about the docker
'''

import requests
import yaml
import os
import paho.mqtt.client as mqtt
import sys
file = open(os.getcwd() + '/config_template.yaml', 'r')
doc = yaml.load(file)
print(doc)
__flask_server_add = str(doc['configuration']['server'])
__flask_port= str(doc['configuration']['port'])
__flask_server = ("http://"+__flask_server_add + ':' + __flask_port)
print("server you are trying to connect to is:"+__flask_server)
headers = {
	"content-type": "application/json",
}

num_of_app = len(doc['configuration']['applications'])

data = {
	"username": str(doc['configuration']['username']),
	"password": str(doc['configuration']['password'])
}
try:
	r = requests.post(__flask_server+'/services/auth', headers=headers, json=data)
	print(data)
# Obtain secret key. It will be attached to all messages here later.
	print("----------- Secret=" + str(r.text)) 
	secret=r.text
	print(num_of_app)
# Create new application
	for app in range(1, num_of_app+1):
		data_app = {
			"secret key": secret,
			"description": str(doc['configuration']['applications']['application'+ str(app)]['description application']),
			"application name": str(doc['configuration']['applications']['application'+ str(app)]['application name']),
		}
		print(data_app)
	#print("t1")
		r_app = requests.post(__flask_server+'/services/createapp', headers=headers, json=data_app)
		print("test1")    
	# Obtain app id
		print("----------- Application created with id=" + str(r_app.text))
		app_id=r_app.text
		print(app_id)
		num_of_devices = len(doc['configuration']['applications']['application'+ str(app)]['devices'])
		for dev in range(1, num_of_devices+1):
		# Create new device and attach it to the created authentication
			data_dev = {
				"secret key": secret,
				"authentication method": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['authentication method']),
				"application id": app_id,
				"description": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['description device']),
				"device eui": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['device eui']),
				"device name": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['device name'])
			}
			r_dev = requests.post(__flask_server+'/services/createdevice', headers=headers, json=data_dev)
			print("----------- Device created" + str(r_dev.content))
			if str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['authentication method']) == "otaa":
				data_config = {
					"secret key": secret,
					"application key": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['application key']),
					"device eui": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['device eui'])
				# For LoRaWAN 1.1.0 uncomment it
				#        "network key": str(doc['network key'])
				
				}
				r_conf = requests.post(__flask_server+'/services/configotaa', headers=headers, json=data_config)
				print("----------- Configuration of device completed" + str(r_conf.content)) 
			elif str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['authentication method']) == "abp":
				data_config = {
					"secret key": secret,
					"app session key": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['app session key']),
					"device address": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['device address']),
					"device eui": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['device eui']),
					"network session key": str(doc['configuration']['applications']['application'+ str(app)]['devices']['device'+str(dev)]['configuration']['network key']) # And delete this
				# For LoRaWAN 1.1.0 uncomment it
				#       "forwarding network session key": str(doc['forwarding network session key']),
				#       "network session encryption key": str(doc['network session encryption key']) 
				#       "network session integrity key": str(doc['network session integrity key'])
				}
				r_conf = requests.post(__flask_server+'/services/configabp', headers=headers, json=data_config)
				print("----------- Configuration of device completed" + str(r_conf.content)) 
except:
	print("couldn't connect to the server.Check address and port in config file or make sure that server is running")
def on_connect(client, userdata, flags, rc):
 print("Connected with result code "+str(rc))
 tpc=("application/"+app_id+"/#")
 #print(tpc)
 client.subscribe(tpc)
def on_message(client, userdata, msg):
 print(msg.topic+" "+str(msg.payload))
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
try:
	client.connect(__flask_server_add, 1883, 60)
except:
	print("could not connect to MQTTSERVER. Please check if the server address is correct and port 1883 is free and running")
client.loop_forever()
print("Ce fini. Goodbye!")
